class Config {
    static isLocal() { return true; }
    static isLogeoDummy() { return false; }
    static getHost() { return '0.0.0.0'; }
    static getPuerto() { return 1237; }
    static getConfigPostgres() {
        return {
            idleTimeoutMillis: 30000,
            max: 25, // max number of clients in the pool
            host: 'postgis',
            port: '5432',
            user: 'postgres',
            password: 'postgres',
            database: 'catastrotij'
        }
    }
    static getPathProyect() {
        return 'file:///C:/catastro_tijuana/';
    }
}

module.exports = Config;