const path = require('path');
const config = require(path.join(__dirname, '/Config'));
const escapee = require('pg-escape');
const request = require("request-promise");

const express = require('express');
const router = express.Router();


var jsreport = require('jsreport')({ 
	allowLocalFilesAccess: true,
	reportTimeout: 60000,
	extensions: {
	  base: {
	    url: config.getPathProyect()	 
	  } 
	},
	logger:{
		silent: true
	}
});
jsreport.init();

const postgres = require("pg");

const config_postgres = config.getConfigPostgres();

//obtener la fecha
const dateTime = require('node-datetime');	

const LAYERS_SNAP = ['construccion', 'manzanas', 'Culiacan_Join']
/*
	// crear un inddex en la tabla que contiene una columna de goemtria
	CREATE INDEX manzanas_geomx ON manzanas USING GIST ( geom );
	CREATE INDEX construccion_geomx ON construccion USING GIST ( geom );
	CREATE INDEX Culiacan_Join_geomx ON "Culiacan_Join" USING GIST ( geom );
	CREATE INDEX colonias_geomx ON colonias USING GIST ( geom );

	CREATE INDEX culiacan_geomx ON culiacan USING GIST ( geom );
	CREATE INDEX urbano_geomx ON urbano USING GIST ( geom );
	CREATE INDEX cuarteles_geomx ON cuarteles USING GIST ( geom );
	CREATE INDEX usos_suelo_geomx ON usos_suelo USING GIST ( geom );
	CREATE INDEX valor_catastral_geomx ON valor_catastral USING GIST ( geom );
	CREATE INDEX zona_valor_geomx ON zona_valor USING GIST ( geom );
	CREATE INDEX laboratorio_valores_geomx ON laboratorio_valores USING GIST ( geom );
	CREATE INDEX vias_comunicacion_geomx ON vias_comunicacion USING GIST ( geom );
	CREATE INDEX hidrografia_geomx ON hidrografia USING GIST ( geom );
	CREATE INDEX uso_suelo_vegetacion_geomx ON uso_suelo_vegetacion USING GIST ( geom );
	vacuum verbose;

	SELECT * FROM geometry_columns;
*/
//uglify-js

router.get('/',(req,res) => {
	req.session.id_usuario = null;
	req.session.permisoPdf = 0; 
	req.session.inicioSesion = true;
	req.session.save();

	var mensageError = '';
	if (req.query.error) {
		switch(req.query.error){
			case '0':
				mensageError = 'El Usuario no existe.';
				break;
			case '2':
				mensageError = 'La contraseña es incorrecta.';
				break;
			case '3':
				mensageError = 'Ocurrio un error al intentar iniciar sesión.';
				break;
			case '4':
				mensageError = 'Se necesita proporcionar correo y contraseña.';
				break;
		}
	}	
  	res.render('login', { error: mensageError});
}); 

router.post('/check',(req,res) => {

	(async () => {	//pdf-creator-node  pdfkit  jsreport
		if (req.body.correo && req.body.pass) {

			try {				

				var options = {
			        uri: "http://127.0.0.1/api/auth.php?user="+req.body.correo+"&password="+req.body.pass+"",
			        method: "GET"
			    }

				resultEncode = await request(options);

				var result = JSON.parse(resultEncode);				

				if (result) {
					if(result.status === 1){			
					
						req.session.id_usuario = result.data[0].id;
						req.session.permisoPdf = result.data[0].privilegio_visor; 
						req.session.inicioSesion = true;
						req.session.save();

						res.writeHead(302, {
						  'Location': '/visor'
						});
						res.end();

					}else{
						res.writeHead(302, {
						  'Location': '/?error=' + result.status
						});
						res.end();
					}
				}else{
					res.writeHead(302, {
					  'Location': '/?error=3'
					});
					res.end();					
				}										

			} catch(e) {
				res.json({"code" : 101, "menssage" : e.message});				
			}

		}else{
			res.writeHead(302, {
			  'Location': '/?error=4'
			});
			res.end();			
		}		
	})();	
});

router.get('/visor',(req,res) => {
	if (req.session.inicioSesion) {
		res.render('visor', {permisoExportPdf: req.session.permisoPdf});
	}else{
		res.writeHead(302, {
		  'Location': '/'
		});
		res.end();
	}
});

router.get('/visor/obtener_features',(req,res) => { 
	(async () => {

		if (req.session.inicioSesion) {

			if (req.query.LAYERS && req.query.p1) {
				//instancia de la base de datos
				var db= new postgres.Pool(config_postgres);
				try {

					var client = await db.connect();
					var resultado = [];
					for (var i = 0; i < req.query.LAYERS.length; i++) {

						//si no existe la tabla en posgres no hacer la consulta.
						var query1 = escapee("SELECT to_regclass('"+req.query.LAYERS[i]+"');");
						var result1 = await client.query(query1);
						if (result1 !== null) {

							var query = null;
							switch(req.query.LAYERS[i]){
								case 'culiacan':
									query = escapee('SELECT dg_mpio, nom_mun FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'urbano':
									query = escapee('SELECT id FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'colonias':
									query = escapee('SELECT dg_mpio, nombre FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'cuarteles':
									query = escapee('SELECT dg_mpio, dg_pobl, dg_ctel FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'manzanas':
									query = escapee('SELECT dg_mpio, dg_pobl, dg_ctel, dg_manz, dg_zona FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'Culiacan_Join':
									query = escapee('SELECT dg_mpio, dg_pobl, dg_ctel, dg_manz, dg_pred, dg_unid, nom_comp_t, ubi_pred, nom_col FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'construccion':
									query = escapee('SELECT "construccion".dg_mpio, "construccion".dg_pobl, "construccion".dg_ctel, "construccion".dg_manz, "construccion".dg_pred, "construccion".dg_unid, "Culiacan_Join".nom_comp_t, "Culiacan_Join".ubi_pred, "Culiacan_Join".nom_col, "construccion".sc, "construccion".v_terr FROM "%s"'+
										' INNER JOIN "Culiacan_Join" ON "construccion".dg_ccat = "Culiacan_Join".dg_ccat '+
										' WHERE '+
										'	st_contains('+
										'		"construccion".geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'usos_suelo':
									query = escapee('SELECT dg_cat, cve_gpo_us, '+
										'CASE cve_gpo_us '+
										'WHEN \'A\' THEN \'Baldios\'   '+ 
										'WHEN \'B\' THEN \'Habitación\'   '+
										'WHEN \'C\' THEN \'Comercio\'   '+
										'WHEN \'D\' THEN \'Edificios Publicos\'  '+
										'WHEN \'E\' THEN \'Escuelas\'  '+
										'WHEN \'F\' THEN \'Parques y Deportes\'  '+
										'WHEN \'G\' THEN \'Industria\'  '+
										'WHEN \'H\' THEN \'Iglesias\' END uso '+
										'FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'valor_catastral':
									query = escapee('SELECT dg_cat, val_catast FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'zona_valor':
									query = escapee('SELECT dg_mpio, dg_pobl, dg_zona, desc_zona, valor FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'laboratorio_valores':
									query = escapee('SELECT dg_zona, desc_zona, valor FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								/*case 'vias_comunicacion':
									query = escapee('SELECT condicion, tipovia, dere_tran, nomvial FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								case 'hidrografia':
									query = escapee('SELECT condicion, entidad FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;*/
								case 'uso_suelo_vegetacion':
									query = escapee('SELECT descripcio FROM "%s"'+
										' WHERE '+
										'	st_contains('+
										'		geom,'+
										'		ST_GeomFromText(\'POINT(%s)\',32613)'+
									')', req.query.LAYERS[i], req.query.p1);
									break;
								default:
									query = null;
									break;
							}

							if (query !== null) {

								var result = await client.query(query);

								if (result.rows.length > 0) {
									resultado.push({"capa" : req.query.LAYERS[i], "data" : result.rows[0]});
								}else{
									resultado.push({"capa" : req.query.LAYERS[i], "data" : null});
								}	
							}else{
								resultado.push({"capa" : req.query.LAYERS[i], "data" : null});
							}						
						}
						
					}

					res.json({"code" : 200, "data" : resultado});
				    client.release();	

				} catch(e) {
					res.json({"code" : 101, "menssage" : e.message});				
				}finally{
					if (db)	db.end();			
				}	
			}else{
				res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
			}	

		}else{
			/*res.writeHead(302, {
			  'Location': '/'
			});
			res.end();*/
			res.json({"code" : 101, "menssage" : "Se necesita iniciar sesión."});
		}	

	})();											
});

router.get('/visor/busquedas',(req,res) => { 
	(async () => {
		if (req.session.inicioSesion) {
		
			if (req.query.buscar && req.query.tipo_busqueda) {
				//instancia de la base de datos
				var db= new postgres.Pool(config_postgres);
				try {

					switch(req.query.tipo_busqueda){
						case "1": //Predio	  007000030245013001						

							if (req.query.buscar.length === 18) {
								//es predio 007000030245013001
								var client = await db.connect();

								var query = escapee('select '+
								'dg_ccat AS clave, '+
								'dg_ccat AS visualizar, '+
								'public.ST_AsGeoJSON("Culiacan_Join".geom,6) AS geometry, '+
								'culiacan.nom_mun AS municipio '+
								'from "Culiacan_Join" '+
								'INNER JOIN culiacan ON "Culiacan_Join".dg_mpio = culiacan.dg_mpio '+
								'where dg_ccat =\'%s\'', req.query.buscar);

								var result = await client.query(query);
								if (result.rows.length > 0) {
									var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'PREDIO', "data": result.rows};
									res.json({"code" : 200, "data" : resultado});

								}else{
									var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'PREDIO', "data": null};
									res.json({"code" : 200, "data" : resultado});
								}

								client.release();

							}else{
								var resultado = {"searchTerm": req.query.buscar, "total": 0, "type": 'PREDIO', "data": null};
								res.json({"code" : 200, "data" : resultado});
							}							
							break;
						case "2": //Manzana  007000030245
							if(req.query.buscar.length === 12){
								//es una manzana 007000030245

								var client = await db.connect();

								var query = escapee('select '+
								'dg_mpcm AS clave, '+
								'dg_mpcm AS visualizar, '+
								'public.ST_AsGeoJSON(manzanas.geom,6) AS geometry, '+
								'culiacan.nom_mun AS municipio '+
								'from manzanas '+
								'INNER JOIN culiacan ON manzanas.dg_mpio = culiacan.dg_mpio '+
								'where dg_mpcm =\'%s\'', req.query.buscar);

								var result = await client.query(query);
								if (result.rows.length > 0) {
									var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'MANZANA', "data": result.rows};
									res.json({"code" : 200, "data" : resultado});
								}else{
									var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'MANZANA', "data": null};
									res.json({"code" : 200, "data" : resultado});
								}

								client.release();
								
							}else{
								var resultado = {"searchTerm": req.query.buscar, "total": 0, "type": 'MANZANA', "data": null};
								res.json({"code" : 200, "data" : resultado});
							}
							break;
						case "3": //Propietario   NUNEZ AISPURO JOSE ANGEL
							//busqueda por nombre del propietario NUNEZ AISPURO JOSE ANGEL
							var client = await db.connect();

							var query = escapee('select '+
							'dg_ccat AS clave, '+
							'dg_ccat AS visualizar, '+
							'public.ST_AsGeoJSON("Culiacan_Join".geom,6) AS geometry, '+
							'culiacan.nom_mun AS municipio '+
							'from "Culiacan_Join" '+
							'INNER JOIN culiacan ON "Culiacan_Join".dg_mpio = culiacan.dg_mpio '+
							'where upper(nom_comp_t) = upper(\'%s\')', req.query.buscar);

							var result = await client.query(query);
							if (result.rows.length > 0) {
								var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'PREDIO', "data": result.rows};
								res.json({"code" : 200, "data" : resultado});

							}else{
								var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'PREDIO', "data": null};
								res.json({"code" : 200, "data" : resultado});
							}

							client.release();
							break;
						case "4": //Cuartel 007000030
							if(req.query.buscar.length === 9){
								//esto es un cuartel  007000030
								var client = await db.connect();

								var query = escapee('select '+
								'idctel AS clave, '+
								'idctel AS visualizar, '+
								'public.ST_AsGeoJSON(cuarteles.geom,6) AS geometry, '+
								'culiacan.nom_mun AS municipio '+
								'from cuarteles '+
								'INNER JOIN culiacan ON cuarteles.dg_mpio = culiacan.dg_mpio '+
								'where idctel =\'_%s\'', req.query.buscar);

								var result = await client.query(query);
								if (result.rows.length > 0) {
									var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'CUARTEL', "data": result.rows};
									res.json({"code" : 200, "data" : resultado});
								}else{
									var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'CUARTEL', "data": null};
									res.json({"code" : 200, "data" : resultado});
								}

								client.release();
							}else{
								var resultado = {"searchTerm": req.query.buscar, "total": 0, "type": 'CUARTEL', "data": null};
								res.json({"code" : 200, "data" : resultado});
							}
							break;
						case "5": //Colonia - Las Cucas
							//busqueda por colonia 
							var client = await db.connect();

							var query = escapee('select '+
							'colonias.id AS clave, '+ //cveasen
							'colonias.nombre AS visualizar, '+
							'public.ST_AsGeoJSON(colonias.geom,6) AS geometry, '+
							'culiacan.nom_mun AS municipio '+
							'from colonias '+
							'INNER JOIN culiacan ON colonias.dg_mpio = culiacan.dg_mpio '+
							'where upper(colonias.nombre) = upper(\'%s\')', req.query.buscar);

							var result = await client.query(query);
							if (result.rows.length > 0) {
								var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'COLONIA', "data": result.rows};
								res.json({"code" : 200, "data" : resultado});

							}else{
								var resultado = {"searchTerm": req.query.buscar, "total": result.rows.length, "type": 'COLONIA', "data": null};
								res.json({"code" : 200, "data" : resultado});
							}

							client.release();
							break;
						default:
							res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
							break;

					}

				} catch(e) {
					res.json({"code" : 101, "menssage" : e.message});				
				}finally{
					if (db)	db.end();			
				}	
			}else{
				res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
			}

		}else{
			res.json({"code" : 101, "menssage" : "Se necesita iniciar sesión."});
		}			
			

	})();											
});

router.post('/visor/generar_pdf_consulta_padron',(req,res) => { 
	(async () => {	//pdf-creator-node  pdfkit  jsreport

		if (req.session.inicioSesion) {

			if (req.session.permisoPdf === 1) {

				if (req.body.mapa && req.body.coordenadas) {
					//instancia de la base de datos
					var db= new postgres.Pool(config_postgres);
					try {

						var client = await db.connect();				

						//si no existe la tabla en posgres no hacer la consulta.	
						const query = escapee('SELECT id, dg_ccat FROM "Culiacan_Join"'+
						' WHERE '+
						'	st_contains('+
						'		geom,'+
						'		ST_GeomFromText(\'POINT(%s)\',32613)'+
						')', req.body.coordenadas);
						
						var resultGeometria = await client.query(query);

						if (resultGeometria.rows.length > 0) {

							const fechaActual = new Date(dateTime.create().format('Y-m-d H:M:S'));
							var content_pdf = `
								<!doctype html>
								    <html>
								       <head>
								            <meta charset="utf-8">
								            <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">							    
								        	<style>

								        		body{
								        			font-family: "Times New Roman", Times, serif;
								        		}

								        		p{
								        			font-size: 12px;
								        		}

								        		strong{
								        			font-size: 12px;
								        		}

								        		#encabezado p{
								        			font-size: 15px;
								        		}

								        		#encabezado strong{
								        			font-size: 15px;
								        		}

								        		#predio p{
								        			font-size: 15px;
								        		}

								        		#predio strong{
								        			font-size: 15px;
								        		}

												.table td, .table th {
												    border-top: none!important;
												    padding: 0!important;
												}

												.table caption {
													text-align: left;
													background: #000;
												}
								        	</style>
								        </head>
								        <body>`;

							content_pdf += 
										`<div id="encabezado">
							            	<img height="110px" width="75px" src="client/recursos/img/logo_direccion_catastro.jpg" style="position: absolute; top: 0;">
								
											<p class="text-center">
							                	<strong>
								                	AYUNTAMIENTO DE CULIACÁN<BR>
								                	DIRECCIÓN DE CATASTRO<BR>
								                	DICTAMEN DE PREDIOS URBANOS
							                	</strong>
							                </p>	
							                <p class="text-center">
							                	<strong>
								                	CONSULTA A PADRON
							                	</strong>
							                </p>
							                <img height="70px" width="120px" src="client/recursos/img/logo_c.png" style="position: absolute; top: 0; right: 95px;">
											<img height="120px" width="90px" src="client/recursos/img/logo_v.png" style="position: absolute; top: 0; right: 0;">
							            </div>
							            <hr>`;

							content_pdf +=`
							            <div id="predio">
								            <p class="text-right" style="margin: 0;">
							                	<strong>
								                	PREDIO:
							                	</strong>
							                	`+resultGeometria.rows[0].dg_ccat+`
							                </p>
							            </div>`;

							//UBICACION            

							const queryUbicacion = escapee(`SELECT 
								AC."CALLE",
								AU."NUM_OFIC_U",
								ACL."COLONIA",
								C.nom_mun,
								CP."NOM_POBL",
								CJ.dg_ctel,
								CJ.dg_manz,
								CJ.dg_pred,
								CJ.dg_unid
								FROM "Culiacan_Join" AS CJ 
								INNER JOIN culiacan as C ON CJ.dg_mpio = C.dg_mpio 
								INNER JOIN "a_pobl" as CP ON CJ.cve_pobl::int = CP."CVE_POBL" 
								INNER JOIN "a_calle" AS AC ON CJ.cve_pobl::int = AC."CVE_POBL" AND CJ.cve_calle::int = AC."CVE_CALLE" 
								INNER JOIN "a_col" AS ACL ON CJ.cve_pobl::int = ACL."CVE_POBL" AND CJ.cve_col::int = ACL."CVE_COL" 
								INNER JOIN "a_unid" AS AU 
								ON CJ.cve_pobl::int = AU."CVE_POBL" 
								AND CJ.num_ctel::int = AU."NUM_CTEL" 
								AND CJ.num_manz::int = AU."NUM_MANZ" 
								AND CJ.num_pred::int = AU."NUM_PRED" 
								AND CJ.num_unid::int = AU."NUM_UNID" 
								WHERE CJ.id = %s 
							`, resultGeometria.rows[0].id);


							const resultUbicacion = await client.query(queryUbicacion);

							if (resultUbicacion.rows.length > 0) {

								content_pdf +=`
							                <div id="ubicacion" style="margin: 0 5mm 0 5mm;">		            			    			  		            						            	
									            		
												<table border="1" style="width: 100%">
												  	<caption><strong><u>UBICACIÓN</u></strong></caption>	
												    <tr>
												      <td><strong>CALLE:</strong> `+resultUbicacion.rows[0].CALLE+` </td>
												      <td><strong>NÚMERO OFICIAL:</strong> `+resultUbicacion.rows[0].NUM_OFIC_U+`</td>
												      <td><strong>COLONIA:</strong> `+resultUbicacion.rows[0].COLONIA+`</td>
												    </tr>
												    <tr>
												      <td><strong>MUNICIPIO:</strong> `+resultUbicacion.rows[0].nom_mun+`</td>
												      <td><strong>POBLACIÓN:</strong> `+resultUbicacion.rows[0].NOM_POBL+`</td>
												      <td><strong>CUARTEL:</strong> `+resultUbicacion.rows[0].dg_ctel+`</td>
												    </tr>
												    <tr>
												      <td><strong>MANZANA:</strong> `+resultUbicacion.rows[0].dg_manz+`</td>
												      <td><strong>NÚMERO PREDIO:</strong> `+resultUbicacion.rows[0].dg_pred+`</td>
												      <td><strong>NÚMERO UNIDAD:</strong> `+resultUbicacion.rows[0].dg_unid+`</td>
												    </tr>
												</table>

											</div>`;

							}



							//PROPIETARIOS

							//consultamos la informacion de propietarios
							const queryPropietarios = escapee(`SELECT 
								AP."TIP_PERS",
								AP."APE_PAT",
								AP."APE_MAT",
								AP."NOM_PROP"
								FROM "Culiacan_Join" AS CJ						
								INNER JOIN "a_unid" AS AU 
								ON CJ.cve_pobl::int = AU."CVE_POBL" 
								AND CJ.num_ctel::int = AU."NUM_CTEL"
								AND CJ.num_manz::int = AU."NUM_MANZ"
								AND CJ.num_pred::int = AU."NUM_PRED"
								AND CJ.num_unid::int = AU."NUM_UNID"
								INNER JOIN "a_prop" AS AP ON AU."CVE_PROP" = AP."CVE_PROP" 
								WHERE CJ.id = %s 
							`, resultGeometria.rows[0].id);


							const resultPropietarios = await client.query(queryPropietarios);

							if (resultPropietarios.rows.length > 0) {

									content_pdf +=`
						                <div id="propietarios" style="margin: 0 5mm 0 5mm;">		            			    			  		            						            	
								            		
											<table border="1" style="width: 100%">
											  <caption><strong><u>PROPIETARIOS</u></strong></caption>							  
											    <tr>
											      <td><strong>TIPO PERSONA:</strong> `+resultPropietarios.rows[0].TIP_PERS+` </td>
											      <td><strong>APELLIDO PATERNO:</strong> `+resultPropietarios.rows[0].APE_PAT+`</td>
											      <td><strong>APELLIDO MATERNO:</strong> `+resultPropietarios.rows[0].APE_MAT+`</td>
											    </tr>
											    <tr>
											      <td><strong>NOMBRE(S):</strong> `+resultPropietarios.rows[0].NOM_PROP+`</td>
											    </tr>
											</table>

										</div>`;
								
							}


							//SUPERFICIES
							const querySuperficies = escapee(`SELECT 
								AU."SUP_TERR",
								AU."SUP_CONST",
								AU."NUM_FTES",
								AU."LON_FTE",
								AU."LON_FONDO"
								FROM "Culiacan_Join" AS CJ						
								INNER JOIN "a_unid" AS AU 
								ON CJ.cve_pobl::int = AU."CVE_POBL" 
								AND CJ.num_ctel::int = AU."NUM_CTEL"
								AND CJ.num_manz::int = AU."NUM_MANZ"
								AND CJ.num_pred::int = AU."NUM_PRED"
								AND CJ.num_unid::int = AU."NUM_UNID"
								WHERE CJ.id = %s 
							`, resultGeometria.rows[0].id);


							const resultSuperficies = await client.query(querySuperficies);

							if (resultSuperficies.rows.length > 0) {							

								content_pdf +=`
						                <div id="superficies" style="margin: 0 5mm 0 5mm;">			            			    			  		            						            	
								            		
											<table border="1" style="width: 100%">	
												<caption><strong><u>SUPERFICIES</u></strong></caption>
											    <tr>
											      <td><strong>SUPERFICIE DE TERRENO:</strong> `+resultSuperficies.rows[0].SUP_TERR+` </td>
											      <td><strong>SUPERFICIE DE CONSTRUCCIÓN:</strong> `+resultSuperficies.rows[0].SUP_CONST+`</td>
											      <td><strong>NÚMERO DE FRENTES:</strong> `+resultSuperficies.rows[0].NUM_FTES+`</td>
											    </tr>
											    <tr>
											      <td><strong>LONGITUD FRENTE(S):</strong> `+resultSuperficies.rows[0].LON_FTE+`</td>
											      <td><strong>LONGITUD FONDO:</strong> `+resultSuperficies.rows[0].LON_FONDO+`</td>
											    </tr>
											</table>

										</div>`;
							}


							//VALORES
							const queryValores = escapee(`SELECT 
								AU."VAL_TERR",
								AU."VAL_CONST"
								FROM "Culiacan_Join" AS CJ						
								INNER JOIN "a_unid" AS AU 
								ON CJ.cve_pobl::int = AU."CVE_POBL" 
								AND CJ.num_ctel::int = AU."NUM_CTEL"
								AND CJ.num_manz::int = AU."NUM_MANZ"
								AND CJ.num_pred::int = AU."NUM_PRED"
								AND CJ.num_unid::int = AU."NUM_UNID"
								WHERE CJ.id = %s 
							`, resultGeometria.rows[0].id);


							const resultValores = await client.query(queryValores);

							if (resultValores.rows.length > 0) {

								content_pdf +=`
					                <div id="valores" style="margin: 0 5mm 0 5mm;">		            			    			  		            						            	
							            		
										<table border="1" style="width: 100%">	
											<caption><strong><u>VALORES</u></strong></caption>	
										    <tr>
										      <td><strong>VALOR DEL TERRENO:</strong> `+resultValores.rows[0].VAL_TERR+`</td>
										      <td><strong>VALOR DE LA CONSTRUCCIÓN:</strong> `+resultValores.rows[0].VAL_CONST+`</td>
										    </tr>
										</table>

									</div>`;
							}

							//SITUACION DEL PREDIO

							const querySituacionPredio = escapee(`SELECT 
								AU."FEC_ALTA",
								AR."DES_REGIM",
								(CASE WHEN AU."TIP_IMPTO" = 1 THEN 'BALDÍO' ELSE 'CONSTRUÍDO' END) AS "tipo_impuesto",
								(CASE WHEN AU."STT_REST" = 'S' THEN 'CON RESTRICCIÓN' ELSE 'SIN RESTRICCIÓN' END) AS "tipo_restriccion"
								FROM "Culiacan_Join" AS CJ						
								INNER JOIN "a_unid" AS AU 
								ON CJ.dg_pobl::int = AU."CVE_POBL" 
								AND CJ.dg_ctel::int = AU."NUM_CTEL"
								AND CJ.dg_manz::int = AU."NUM_MANZ"
								AND CJ.dg_pred::int = AU."NUM_PRED"
								AND CJ.dg_unid::int = AU."NUM_UNID"
								INNER JOIN "a_regim" AS AR ON AR."CVE_REGIM" = AU."CVE_REGIM" 
								WHERE CJ.id = %s 
							`, resultGeometria.rows[0].id);
							
							const resultSituacionPredio = await client.query(querySituacionPredio);

							if (resultSituacionPredio.rows.length > 0) {

								content_pdf +=`
					                <div id="situacion_del_predio" style="margin: 0 5mm 0 5mm;">		            			    			  		            						            	
							            		
										<table border="1" style="width: 100%">
											<caption><strong><u>SITUACIÓN DEL PREDIO</u></strong></caption>	
										    <tr>
										      <td><strong>FECHA ALTA:</strong> `+resultSituacionPredio.rows[0].FEC_ALTA+`</td>
										      <td><strong>TIPO RÉGIMEN:</strong> `+resultSituacionPredio.rows[0].DES_REGIM+`</td>
										      <td><strong>TIPO IMPUESTO:</strong> `+resultSituacionPredio.rows[0].tipo_impuesto+`</td>
										    </tr>
										    <tr>
										      <td><strong>TIPO RESTRICCIÓN:</strong> `+resultSituacionPredio.rows[0].tipo_restriccion+`</td>
										    </tr>
										</table>

									</div>`;

							}


							//SERVICIOS
							const queryServicios = escapee(`SELECT 
								AU."STS_SERVIC" AS servicios
								FROM "Culiacan_Join" AS CJ						
								INNER JOIN "a_unid" AS AU 
								ON CJ.dg_pobl::int = AU."CVE_POBL" 
								AND CJ.dg_ctel::int = AU."NUM_CTEL"
								AND CJ.dg_manz::int = AU."NUM_MANZ"
								AND CJ.dg_pred::int = AU."NUM_PRED"
								AND CJ.dg_unid::int = AU."NUM_UNID"
								WHERE CJ.id = %s 
							`, resultGeometria.rows[0].id);

							const resultServicios = await client.query(queryServicios);

							if (resultServicios.rows.length > 0) {

								const datoServicio = resultServicios.rows[0].servicios; //'13111111100'
								const grupoServicios = [
													 'AGUA',
													 'ALUMBRADO',
													 'BANQUETA',
													 'DRENAJE', 
													 'DESAGUE', 
													 'ENERGIA', 
													 'GUARNICION', 
													 'PAVIMENTO', 
													 'TOMA DE AGUA'];

								var arrayServicios = [];

								for (var i = 0; i < datoServicio.length; i++) {
									if (i === 1 || i === 7) {

										if (i === 1) {
											var des_servicio = null;
											switch(datoServicio[i]){
												case '0':
													des_servicio = 'ALUMBRADO';
													break;
												case '1':
													des_servicio = 'MERCURIAL';
													break;
												case '2':
													des_servicio = 'FLUORESCENTE';
													break;
												case '3':
													des_servicio = 'INCANDESCENTE';
													break;
												case '4':
													des_servicio = 'NO HAY';
													break;
												case 'A':
													des_servicio = 'MERCURIAL-FLUORESCENTE';
													break;
												case 'B':
													des_servicio = 'MERCURIAL-INCANDESCENTE';
													break;
												case 'C':
													des_servicio = 'MERCURIAL-NO HAY';
													break;
												case 'D':
													des_servicio = 'FLUORESCENTE-INCANDESCENTE';
													break;
												default:
													des_servicio = null;
													break;

											}
											if (des_servicio !== null) {
												arrayServicios.push(des_servicio);
											}
										}else{

											var des_servicio = null;
											switch(datoServicio[i]){
												case '0':
													des_servicio = 'PAVIMENTO';
													break;
												case '1':
													des_servicio = 'CONCRETO';
													break;
												case '2':
													des_servicio = 'ASFALTO';
													break;
												case '3':
													des_servicio = 'EMPEDRADO';
													break;
												case '4':
													des_servicio = 'NATURAL';
													break;
												case 'A':
													des_servicio = 'CONCRETO-ASFALTO';
													break;
												case 'B':
													des_servicio = 'CONCRETO-EMPEDRADO';
													break;
												case 'C':
													des_servicio = 'CONCRETO-NATURAL';
													break;
												case 'D':
													des_servicio = 'ASFALTO-EMPEDRADO';
													break;
												case 'E':
													des_servicio = 'ASFALTO-NATURAL';
													break;
												case 'F':
													des_servicio = 'EMPEDRADO-NATURAL';
													break;
												default:
													des_servicio = null;
													break;

											}
											if (des_servicio !== null) {
												arrayServicios.push(des_servicio);
											}
										}

									}else{
										if (datoServicio[i] > 0) {
											arrayServicios.push(grupoServicios[i]);
										}
									}
								}

								content_pdf +=`
					                <div id="servicios" style="margin: 0 5mm 0 5mm;">			            			    			  		            						            	
							            		
										<table border="1" style="width: 100%">	
										  <caption><strong><u>SERVICIOS</u></strong></caption>
										    <tr>
										      <td><strong>TIPO SERVICIOS:</strong> `+arrayServicios.toString()+`</td>
										    </tr>
										</table>

									</div>`;
							}


							//USOS
							const queryUsos = escapee(`SELECT 
								AG."DES_GPO_US" AS "grupo_uso",
								AG."DES_GPO_US" AS "uso"
								FROM "Culiacan_Join" AS CJ						
								INNER JOIN "a_uso_u" AS AU 							
								ON CJ.dg_pobl::int = AU."CVE_POBL" 
								AND CJ.dg_ctel::int = AU."NUM_CTEL"
								AND CJ.dg_manz::int = AU."NUM_MANZ"
								AND CJ.dg_pred::int = AU."NUM_PRED"
								AND CJ.dg_unid::int = AU."NUM_UNID"
								AND CJ.cve_gpo_us = AU."CVE_GPO_USO"
								INNER JOIN "a_gUso" AS AG ON AG."CVE_GPO_US" = AU."CVE_GPO_USO" 
								WHERE CJ.id = %s 
							`, resultGeometria.rows[0].id);

							const resultUsos = await client.query(queryUsos);

							if (resultUsos.rows.length > 0) {
								content_pdf +=`
					                <div id="usos" style="margin: 0 5mm 0 5mm;">		            			    			  		            						            	
							            		
										<table border="1" style="width: 100%">	
										  <caption><strong><u>USOS</u></strong></caption>		
										    <tr>
										      <td><strong>GRUPO USO:</strong> `+resultUsos.rows[0].grupo_uso+`</td>
										      <td><strong>USO:</strong> `+resultUsos.rows[0].uso+`</td>
										    </tr>
										</table>

									</div>`;
							        
							}


							//CONSTRUCCIONES
							const queryConstrucciones = escapee(`SELECT 
								AC."NUM_CONST",
								AC."SUP_CONST",
								AC."CVE_CAT_CO",
								CONCAT('EDO-', AC."CVE_EDO_CO") AS "estado_conservacion",
								AC."EDD_CONST",
								AC."FEC_CONST"
								FROM "Culiacan_Join" AS CJ						
								INNER JOIN "a_const" AS AC							
								ON CONCAT(CJ.dg_pobl, CJ.dg_ctel, CJ.dg_manz, CJ.dg_pred, CJ.dg_unid) = AC.clave 
								WHERE CJ.id = %s 
							`, resultGeometria.rows[0].id);

							const resultConstrucciones = await client.query(queryConstrucciones);

							if (resultConstrucciones.rows.length > 0) {

								content_pdf +=`								
					                <div id="construcciones" style="margin: 0 5mm 0 5mm;">
					                	<table border="1" style="width: 100%;">	
					                		<caption><strong><u>CONSTRUCCIONES</u></strong></caption>
					                		<tr>
										      <th><strong>NÚMERO DE CONSTRUCCIÓN</strong></th>
										      <th><strong>SUPERFICIE DE CONSTRUCCIÓN</strong></th>
										      <th><strong>CATEGORÍA DE CONSTRUCCIÓN</strong></th>
										      <th><strong>ESTADO DE CONSERVACIÓN</strong></th>
										      <th><strong>EDAD DE LA CONSTRUCCIÓN</strong></th>
										      <th><strong>FECHA DE CONSTRUCCIÓN</strong></th>									    
										    </tr>
						            	`;

					            resultConstrucciones.rows.forEach(function(construccion) {
								 	
								 	content_pdf +=`	
										
										    <tr>
										      <td>`+construccion.NUM_CONST+`</td>
										      <td>`+construccion.SUP_CONST+`</td>
										      <td>`+construccion.CVE_CAT_CO+`</td>
										      <td><`+construccion.estado_conservacion+`</td>
										      <td>`+construccion.EDD_CONST+`</td>
										      <td>`+construccion.FEC_CONST+`</td>
										    </tr>
										`;

								});  

								content_pdf +=`	</table>
													</div>`;
							}

							//MAPA
							content_pdf +=`
										<div id="mapa">
											<div style="page-break-before: always;"></div>
								            <p class="text-center" style="margin: 10px 0 10px 0;">
							                	<strong>
								                	UBICACIÓN DEL PREDIO EN EL MAPA
							                	</strong>
							                </p>

								            <div class="text-center">    
								            	<img src="`+req.body.mapa+`" >
								            </div>
								        </div>		
								        </body>
								    </html>`;


					    	client.release();
							jsreport.render({
								template: {
								  content: content_pdf,							 
								  engine: 'handlebars',
								  recipe: 'chrome-pdf', //html  chrone-pdf
								  chrome: {
								  	scale: 1,
								  	displayHeaderFooter: true,
							        format: 'Letter', //Letter
							        printBackground: true,
							        marginTop: '8mm',
							        marginRight: '8mm',
							        marginBottom: '4mm',
							        marginLeft: '8mm'
								  }
								}
							}).then((out)  => {
								var filename = "consultaPadron.pdf"; 					
								res.setHeader('Content-Disposition', 'inline; filename=' + filename); ////attachment
								res.setHeader('Content-type', 'application/pdf');
								out.stream.pipe(res);
							});


						}else{	
					    	client.release();	
							res.json({"code" : 101, "menssage" : "Se produjo un error al obtener la información para generar el pdf22."});
						}			
											

					} catch(e) {
						res.json({"code" : 101, "menssage" : e.message});				
					}finally{
						if (db)	db.end();			
					}	
				}else{
					res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
				}
			}else{
				res.json({"code" : 101, "menssage" : "No tiene permiso para generar el PDF."});
			}	

		}else{
			res.json({"code" : 101, "menssage" : "Se necesita iniciar sesión."});
		}
	})();	
});

/*router.post('/visor/generar_pdf_consulta_padron',(req,res) => { 
	(async () => {	//pdf-creator-node  pdfkit  jsreport

		if (req.body.mapa && req.body.coordenadas) {
			//instancia de la base de datos
			var db= new postgres.Pool(config_postgres);
			try {

				var client = await db.connect();				

				//si no existe la tabla en posgres no hacer la consulta.	
				const query = escapee('SELECT id, dg_ccat FROM "Culiacan_Join"'+
				' WHERE '+
				'	st_contains('+
				'		geom,'+
				'		ST_GeomFromText(\'POINT(%s)\',32613)'+
				')', req.body.coordenadas);
				
				var resultGeometria = await client.query(query);

				if (resultGeometria.rows.length > 0) {

					const fechaActual = new Date(dateTime.create().format('Y-m-d H:M:S'));
					var content_pdf = `
						<!doctype html>
						    <html>
						       <head>
						            <meta charset="utf-8">
						            <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">							    
						        	<style>

						        		body{
						        			font-family: "Times New Roman", Times, serif;
						        		}

						        		p{
						        			font-size: 15px;
						        		}

						        		strong{
						        			font-size: 15px;
						        		}

						        		#encabezado p{
						        			font-size: 17px;
						        		}

						        		#encabezado strong{
						        			font-size: 17px;
						        		}

										.table td, .table th {
										    border-top: none!important;
										    padding: 0!important;
										}
						        	</style>
						        </head>
						        <body>`;

					content_pdf += 
								`<div id="encabezado">
					            	<img height="110px" width="75px" src="client/recursos/img/logo_direccion_catastro.jpg" style="position: absolute; top: 0;">
						
									<p class="text-center">
					                	<strong>
						                	AYUNTAMIENTO DE CULIACÁN<BR>
						                	DIRECCIÓN DE CATASTRO<BR>
						                	DICTAMEN DE PREDIOS URBANOS
					                	</strong>
					                </p>	
					                <p class="text-center">
					                	<strong>
						                	CONSULTA A PADRON
					                	</strong>
					                </p>
					                <img height="70px" width="120px" src="client/recursos/img/logo_c.png" style="position: absolute; top: 0; right: 95px;">
									<img height="120px" width="90px" src="client/recursos/img/logo_v.png" style="position: absolute; top: 0; right: 0;">
					            </div>
					            <hr>`;

					content_pdf +=`
					            <div id="predio">
						            <p class="text-center" style="margin: 0;">
					                	<strong>
						                	PREDIO:
					                	</strong>
					                	`+resultGeometria.rows[0].dg_ccat+`
					                </p>
					            </div>`;

					//UBICACION            

					const queryUbicacion = escapee(`SELECT 
						AC."CALLE",
						AU."NUM_OFIC_U",
						ACL."COLONIA",
						C.nom_mun,
						CP."NOM_POBL",
						CJ.dg_ctel,
						CJ.dg_manz,
						CJ.dg_pred,
						CJ.dg_unid
						FROM "Culiacan_Join" AS CJ 
						INNER JOIN culiacan as C ON CJ.dg_mpio = C.dg_mpio 
						INNER JOIN "a_pobl" as CP ON CJ.cve_pobl::int = CP."CVE_POBL" 
						INNER JOIN "a_calle" AS AC ON CJ.cve_pobl::int = AC."CVE_POBL" AND CJ.cve_calle::int = AC."CVE_CALLE" 
						INNER JOIN "a_col" AS ACL ON CJ.cve_pobl::int = ACL."CVE_POBL" AND CJ.cve_col::int = ACL."CVE_COL" 
						INNER JOIN "a_unid" AS AU 
						ON CJ.cve_pobl::int = AU."CVE_POBL" 
						AND CJ.num_ctel::int = AU."NUM_CTEL" 
						AND CJ.num_manz::int = AU."NUM_MANZ" 
						AND CJ.num_pred::int = AU."NUM_PRED" 
						AND CJ.num_unid::int = AU."NUM_UNID" 
						WHERE CJ.id = %s 
					`, resultGeometria.rows[0].id);


					const resultUbicacion = await client.query(queryUbicacion);

					if (resultUbicacion.rows.length > 0) {

						content_pdf +=`
					                <div id="ubicacion" style="margin: 0 5mm 0 5mm;">

						            	<p style="margin: 0; margin-top: 2mm;">
						                	<strong><u>
							                	UBICACIÓN
						                	</u></strong>
						                </p>			            			    			  		            						            	
							            		
										<table style="margin: 5mm 0 5mm 0; width: 100%">
										    <colgroup>
										       <col span="1" style="width: 50%;">
										       <col span="1" style="width: 50%;">
										    </colgroup>							  
										  <tbody>
										    <tr style="vertical-align: baseline;">
										      <td><strong>CALLE:</strong> `+resultUbicacion.rows[0].CALLE+` </td>
										      <td><strong>NÚMERO OFICIAL:</strong> `+resultUbicacion.rows[0].NUM_OFIC_U+`</td>
										    </tr>
										    <tr style="vertical-align: baseline;">
										      <td style="width: 50%;"><strong>COLONIA:</strong> `+resultUbicacion.rows[0].COLONIA+`</td>
										      <td style="width: 50%;"><strong>MUNICIPIO:</strong> `+resultUbicacion.rows[0].nom_mun+`</td>
										    </tr>
										    <tr style="vertical-align: baseline;">
										      <td style="width: 50%;"><strong>POBLACIÓN:</strong> `+resultUbicacion.rows[0].NOM_POBL+`</td>
										      <td style="width: 50%;"><strong>CUARTEL:</strong> `+resultUbicacion.rows[0].dg_ctel+`</td>
										    </tr>
										    <tr style="vertical-align: baseline;">
										      <td style="width: 50%;"><strong>MANZANA:</strong> `+resultUbicacion.rows[0].dg_manz+`</td>
										      <td style="width: 50%;"><strong>NÚMERO PREDIO:</strong> `+resultUbicacion.rows[0].dg_pred+`</td>
										    </tr>
										    <tr style="vertical-align: baseline;">
										      <td style="width: 50%;"><strong>NÚMERO UNIDAD:</strong> `+resultUbicacion.rows[0].dg_unid+`</td>
										      <td style="width: 50%;"></td>
										    </tr>
										  </tbody>
										</table>

									</div>`;
					}



					//PROPIETARIOS

					//consultamos la informacion de propietarios
					const queryPropietarios = escapee(`SELECT 
						AP."TIP_PERS",
						AP."APE_PAT",
						AP."APE_MAT",
						AP."NOM_PROP"
						FROM "Culiacan_Join" AS CJ						
						INNER JOIN "a_unid" AS AU 
						ON CJ.cve_pobl::int = AU."CVE_POBL" 
						AND CJ.num_ctel::int = AU."NUM_CTEL"
						AND CJ.num_manz::int = AU."NUM_MANZ"
						AND CJ.num_pred::int = AU."NUM_PRED"
						AND CJ.num_unid::int = AU."NUM_UNID"
						INNER JOIN "a_prop" AS AP ON AU."CVE_PROP" = AP."CVE_PROP" 
						WHERE CJ.id = %s 
					`, resultGeometria.rows[0].id);


					const resultPropietarios = await client.query(queryPropietarios);

					if (resultPropietarios.rows.length > 0) {

							content_pdf +=`
				                <div id="propietarios" style="margin: 0 5mm 0 5mm;">

					            	<p style="margin: 0; margin-top: 2mm;">
					                	<strong><u>
						                	PROPIETARIOS
					                	</u></strong>
					                </p>			            			    			  		            						            	
						            		
									<table style="margin: 5mm 0 5mm 0; width: 100%">
									    <colgroup>
									       <col span="1" style="width: 50%;">
									       <col span="1" style="width: 50%;">
									    </colgroup>							  
									  <tbody>
									    <tr style="vertical-align: baseline;">
									      <td><strong>TIPO PERSONA:</strong> `+resultPropietarios.rows[0].TIP_PERS+` </td>
									      <td><strong>APELLIDO PATERNO:</strong> `+resultPropietarios.rows[0].APE_PAT+`</td>
									    </tr>
									    <tr style="vertical-align: baseline;">
									      <td style="width: 50%;"><strong>APELLIDO MATERNO:</strong> `+resultPropietarios.rows[0].APE_MAT+`</td>
									      <td style="width: 50%;"><strong>NOMBRE(S):</strong> `+resultPropietarios.rows[0].NOM_PROP+`</td>
									    </tr>
									  </tbody>
									</table>

								</div>`;
						
					}


					//SUPERFICIES
					const querySuperficies = escapee(`SELECT 
						AU."SUP_TERR",
						AU."SUP_CONST",
						AU."NUM_FTES",
						AU."LON_FTE",
						AU."LON_FONDO"
						FROM "Culiacan_Join" AS CJ						
						INNER JOIN "a_unid" AS AU 
						ON CJ.cve_pobl::int = AU."CVE_POBL" 
						AND CJ.num_ctel::int = AU."NUM_CTEL"
						AND CJ.num_manz::int = AU."NUM_MANZ"
						AND CJ.num_pred::int = AU."NUM_PRED"
						AND CJ.num_unid::int = AU."NUM_UNID"
						WHERE CJ.id = %s 
					`, resultGeometria.rows[0].id);


					const resultSuperficies = await client.query(querySuperficies);

					if (resultSuperficies.rows.length > 0) {							

						content_pdf +=`
				                <div id="superficies" style="margin: 0 5mm 0 5mm;">

					            	<p style="margin: 0; margin-top: 2mm;">
					                	<strong><u>
						                	SUPERFICIES
					                	</u></strong>
					                </p>			            			    			  		            						            	
						            		
									<table style="margin: 5mm 0 5mm 0; width: 100%">
									    <colgroup>
									       <col span="1" style="width: 50%;">
									       <col span="1" style="width: 50%;">
									    </colgroup>							  
									  <tbody>
									    <tr style="vertical-align: baseline;">
									      <td><strong>SUPERFICIE DE TERRENO:</strong> `+resultSuperficies.rows[0].SUP_TERR+` </td>
									      <td><strong>SUPERFICIE DE CONSTRUCCIÓN:</strong> `+resultSuperficies.rows[0].SUP_CONST+`</td>
									    </tr>
									    <tr style="vertical-align: baseline;">
									      <td style="width: 50%;"><strong>NÚMERO DE FRENTES:</strong> `+resultSuperficies.rows[0].NUM_FTES+`</td>
									      <td style="width: 50%;"><strong>LONGITUD FRENTE(S):</strong> `+resultSuperficies.rows[0].LON_FTE+`</td>
									    </tr>
									    <tr style="vertical-align: baseline;">
									      <td style="width: 50%;"><strong>LONGITUD FONDO:</strong> `+resultSuperficies.rows[0].LON_FONDO+`</td>
									      <td style="width: 50%;"></td>
									    </tr>
									  </tbody>
									</table>

								</div>`;
					}


					//VALORES
					const queryValores = escapee(`SELECT 
						AU."VAL_TERR",
						AU."VAL_CONST"
						FROM "Culiacan_Join" AS CJ						
						INNER JOIN "a_unid" AS AU 
						ON CJ.cve_pobl::int = AU."CVE_POBL" 
						AND CJ.num_ctel::int = AU."NUM_CTEL"
						AND CJ.num_manz::int = AU."NUM_MANZ"
						AND CJ.num_pred::int = AU."NUM_PRED"
						AND CJ.num_unid::int = AU."NUM_UNID"
						WHERE CJ.id = %s 
					`, resultGeometria.rows[0].id);


					const resultValores = await client.query(queryValores);

					if (resultValores.rows.length > 0) {

						content_pdf +=`
			                <div id="valores" style="margin: 0 5mm 0 5mm;">

				            	<p style="margin: 0; margin-top: 2mm;">
				                	<strong><u>
					                	VALORES
				                	</u></strong>
				                </p>			            			    			  		            						            	
					            		
								<table style="margin: 5mm 0 5mm 0; width: 100%">
								    <colgroup>
								       <col span="1" style="width: 50%;">
								       <col span="1" style="width: 50%;">
								    </colgroup>							  
								  <tbody>
								    <tr style="vertical-align: baseline;">
								      <td><strong>VALOR DEL TERRENO:</strong> `+resultValores.rows[0].VAL_TERR+`</td>
								      <td><strong>VALOR DE LA CONSTRUCCIÓN:</strong> `+resultValores.rows[0].VAL_CONST+`</td>
								    </tr>
								  </tbody>
								</table>

							</div>`;
					}

					//SITUACION DEL PREDIO

					const querySituacionPredio = escapee(`SELECT 
						AU."FEC_ALTA",
						AR."DES_REGIM",
						(CASE WHEN AU."TIP_IMPTO" = 1 THEN 'BALDÍO' ELSE 'CONSTRUÍDO' END) AS "tipo_impuesto",
						(CASE WHEN AU."STT_REST" = 'S' THEN 'CON RESTRICCIÓN' ELSE 'SIN RESTRICCIÓN' END) AS "tipo_restriccion"
						FROM "Culiacan_Join" AS CJ						
						INNER JOIN "a_unid" AS AU 
						ON CJ.dg_pobl::int = AU."CVE_POBL" 
						AND CJ.dg_ctel::int = AU."NUM_CTEL"
						AND CJ.dg_manz::int = AU."NUM_MANZ"
						AND CJ.dg_pred::int = AU."NUM_PRED"
						AND CJ.dg_unid::int = AU."NUM_UNID"
						INNER JOIN "a_regim" AS AR ON AR."CVE_REGIM" = AU."CVE_REGIM" 
						WHERE CJ.id = %s 
					`, resultGeometria.rows[0].id);
					
					const resultSituacionPredio = await client.query(querySituacionPredio);

					if (resultSituacionPredio.rows.length > 0) {

						content_pdf +=`
			                <div id="situacion_del_predio" style="margin: 0 5mm 0 5mm;">

				            	<p style="margin: 0; margin-top: 2mm;">
				                	<strong><u>
					                	SITUACIÓN DEL PREDIO
				                	</u></strong>
				                </p>			            			    			  		            						            	
					            		
								<table style="margin: 5mm 0 5mm 0; width: 100%">
								    <colgroup>
								       <col span="1" style="width: 50%;">
								       <col span="1" style="width: 50%;">
								    </colgroup>							  
								  <tbody>
								    <tr style="vertical-align: baseline;">
								      <td><strong>FECHA ALTA:</strong> `+resultSituacionPredio.rows[0].FEC_ALTA+`</td>
								      <td><strong>TIPO RÉGIMEN:</strong> `+resultSituacionPredio.rows[0].DES_REGIM+`</td>
								    </tr>
								    <tr style="vertical-align: baseline;">
								      <td><strong>TIPO IMPUESTO:</strong> `+resultSituacionPredio.rows[0].tipo_impuesto+`</td>
								      <td><strong>TIPO RESTRICCIÓN:</strong> `+resultSituacionPredio.rows[0].tipo_restriccion+`</td>
								    </tr>
								  </tbody>
								</table>

							</div>`;

					}


					//SERVICIOS
					const queryServicios = escapee(`SELECT 
						AU."STS_SERVIC" AS servicios
						FROM "Culiacan_Join" AS CJ						
						INNER JOIN "a_unid" AS AU 
						ON CJ.dg_pobl::int = AU."CVE_POBL" 
						AND CJ.dg_ctel::int = AU."NUM_CTEL"
						AND CJ.dg_manz::int = AU."NUM_MANZ"
						AND CJ.dg_pred::int = AU."NUM_PRED"
						AND CJ.dg_unid::int = AU."NUM_UNID"
						WHERE CJ.id = %s 
					`, resultGeometria.rows[0].id);

					const resultServicios = await client.query(queryServicios);

					if (resultServicios.rows.length > 0) {

						const datoServicio = resultServicios.rows[0].servicios; //'13111111100'
						const grupoServicios = [
											 'AGUA',
											 'ALUMBRADO',
											 'BANQUETA',
											 'DRENAJE', 
											 'DESAGUE', 
											 'ENERGIA', 
											 'GUARNICION', 
											 'PAVIMENTO', 
											 'TOMA DE AGUA'];

						var arrayServicios = [];

						for (var i = 0; i < datoServicio.length; i++) {
							if (i === 1 || i === 7) {

								if (i === 1) {
									var des_servicio = null;
									switch(datoServicio[i]){
										case '0':
											des_servicio = 'ALUMBRADO';
											break;
										case '1':
											des_servicio = 'MERCURIAL';
											break;
										case '2':
											des_servicio = 'FLUORESCENTE';
											break;
										case '3':
											des_servicio = 'INCANDESCENTE';
											break;
										case '4':
											des_servicio = 'NO HAY';
											break;
										case 'A':
											des_servicio = 'MERCURIAL-FLUORESCENTE';
											break;
										case 'B':
											des_servicio = 'MERCURIAL-INCANDESCENTE';
											break;
										case 'C':
											des_servicio = 'MERCURIAL-NO HAY';
											break;
										case 'D':
											des_servicio = 'FLUORESCENTE-INCANDESCENTE';
											break;
										default:
											des_servicio = null;
											break;

									}
									if (des_servicio !== null) {
										arrayServicios.push(des_servicio);
									}
								}else{

									var des_servicio = null;
									switch(datoServicio[i]){
										case '0':
											des_servicio = 'PAVIMENTO';
											break;
										case '1':
											des_servicio = 'CONCRETO';
											break;
										case '2':
											des_servicio = 'ASFALTO';
											break;
										case '3':
											des_servicio = 'EMPEDRADO';
											break;
										case '4':
											des_servicio = 'NATURAL';
											break;
										case 'A':
											des_servicio = 'CONCRETO-ASFALTO';
											break;
										case 'B':
											des_servicio = 'CONCRETO-EMPEDRADO';
											break;
										case 'C':
											des_servicio = 'CONCRETO-NATURAL';
											break;
										case 'D':
											des_servicio = 'ASFALTO-EMPEDRADO';
											break;
										case 'E':
											des_servicio = 'ASFALTO-NATURAL';
											break;
										case 'F':
											des_servicio = 'EMPEDRADO-NATURAL';
											break;
										default:
											des_servicio = null;
											break;

									}
									if (des_servicio !== null) {
										arrayServicios.push(des_servicio);
									}
								}

							}else{
								if (datoServicio[i] > 0) {
									arrayServicios.push(grupoServicios[i]);
								}
							}
						}

						content_pdf +=`
			                <div id="servicios" style="margin: 0 5mm 0 5mm;">

				            	<p style="margin: 0; margin-top: 2mm;">
				                	<strong><u>
					                	SERVICIOS
				                	</u></strong>
				                </p>			            			    			  		            						            	
					            		
								<table style="margin: 5mm 0 5mm 0; width: 100%">
								    <colgroup>
								       <col span="1" style="width: 100%;">
								    </colgroup>							  
								  <tbody>
								    <tr style="vertical-align: baseline;">
								      <td><strong>TIPO SERVICIOS:</strong> `+arrayServicios.toString()+`</td>
								    </tr>
								  </tbody>
								</table>

							</div>`;
					}


					//USOS
					const queryUsos = escapee(`SELECT 
						AG."DES_GPO_US" AS "grupo_uso",
						AG."DES_GPO_US" AS "uso"
						FROM "Culiacan_Join" AS CJ						
						INNER JOIN "a_uso_u" AS AU 							
						ON CJ.dg_pobl::int = AU."CVE_POBL" 
						AND CJ.dg_ctel::int = AU."NUM_CTEL"
						AND CJ.dg_manz::int = AU."NUM_MANZ"
						AND CJ.dg_pred::int = AU."NUM_PRED"
						AND CJ.dg_unid::int = AU."NUM_UNID"
						AND CJ.cve_gpo_us = AU."CVE_GPO_USO"
						INNER JOIN "a_gUso" AS AG ON AG."CVE_GPO_US" = AU."CVE_GPO_USO" 
						WHERE CJ.id = %s 
					`, resultGeometria.rows[0].id);

					const resultUsos = await client.query(queryUsos);

					if (resultUsos.rows.length > 0) {
						content_pdf +=`
			                <div id="usos" style="margin: 0 5mm 0 5mm;">

				            	<p style="margin: 0; margin-top: 2mm;">
				                	<strong><u>
					                	USOS
				                	</u></strong>
				                </p>			            			    			  		            						            	
					            		
								<table style="margin: 5mm 0 5mm 0; width: 100%">
								    <colgroup>
								       <col span="1" style="width: 50%;">
								       <col span="1" style="width: 50%;">
								    </colgroup>							  
								  <tbody>
								    <tr style="vertical-align: baseline;">
								      <td><strong>GRUPO USO:</strong> `+resultUsos.rows[0].grupo_uso+`</td>
								      <td><strong>USO:</strong> `+resultUsos.rows[0].uso+`</td>
								    </tr>
								  </tbody>
								</table>

							</div>
							<div style="page-break-before: always;"></div>
							`;
					        
					}


					//CONSTRUCCIONES
					const queryConstrucciones = escapee(`SELECT 
						AC."NUM_CONST",
						AC."SUP_CONST",
						AC."CVE_CAT_CO",
						CONCAT('EDO-', AC."CVE_EDO_CO") AS "estado_conservacion",
						AC."EDD_CONST",
						AC."FEC_CONST"
						FROM "Culiacan_Join" AS CJ						
						INNER JOIN "a_const" AS AC							
						ON CONCAT(CJ.dg_pobl, CJ.dg_ctel, CJ.dg_manz, CJ.dg_pred, CJ.dg_unid) = AC.clave 
						WHERE CJ.id = %s 
					`, resultGeometria.rows[0].id);

					const resultConstrucciones = await client.query(queryConstrucciones);

					if (resultConstrucciones.rows.length > 0) {

						content_pdf +=`								
			                <div id="construcciones" style="margin: 0 5mm 0 5mm;">

				            	<p style="margin: 0; margin-top: 2mm;">
				                	<strong><u>
					                	CONSTRUCCIONES
				                	</u></strong>
				                </p>`;

			            resultConstrucciones.rows.forEach(function(construccion) {
						 	
						 	content_pdf +=`	
								<table style="margin: 5mm 0 5mm 0; width: 100%; border: 1px solid silver;">
								    <colgroup>
								       <col span="1" style="width: 50%;">
								       <col span="1" style="width: 50%;">
								    </colgroup>							  
								  <tbody>
								    <tr style="vertical-align: baseline;">
								      <td><strong>NÚMERO DE CONSTRUCCIÓN:</strong> `+construccion.NUM_CONST+`</td>
								      <td><strong>SUPERFICIE DE CONSTRUCCIÓN:</strong> `+construccion.SUP_CONST+`</td>
								    </tr>
								   	<tr style="vertical-align: baseline;">
								      <td><strong>CATEGORÍA DE CONSTRUCCIÓN:</strong> `+construccion.CVE_CAT_CO+`</td>
								      <td><strong>ESTADO DE CONSERVACIÓN:</strong> `+construccion.estado_conservacion+`</td>
								    </tr>
								    <tr style="vertical-align: baseline;">
								      <td><strong>EDAD DE LA CONSTRUCCIÓN:</strong> `+construccion.EDD_CONST+`</td>
								      <td><strong>FECHA DE CONSTRUCCIÓN:</strong> `+construccion.FEC_CONST+`</td>
								    </tr>
								  </tbody>
								</table>`;

						});  

						content_pdf +=`	</div>`;
					}

					//MAPA
					content_pdf +=`
								<div id="mapa">

						            <p class="text-center" style="margin: 0 0 10px 0;">
					                	<strong>
						                	UBICACIÓN DEL PREDIO EN EL MAPA
					                	</strong>
					                </p>

						            <div class="text-center">    
						            	<img src="`+req.body.mapa+`" >
						            </div>
						        </div>		
						        </body>
						    </html>`;


			    	client.release();
					jsreport.render({
						template: {
						  content: content_pdf,							 
						  engine: 'handlebars',
						  recipe: 'chrome-pdf', //html  chrone-pdf
						  chrome: {
						  	scale: 1,
						  	displayHeaderFooter: true,
					        format: 'Letter', //Letter
					        printBackground: true,
					        marginTop: '8mm',
					        marginRight: '8mm',
					        marginBottom: '4mm',
					        marginLeft: '8mm'
						  }
						}
					}).then((out)  => {
						var filename = "consultaPadron.pdf"; 					
						res.setHeader('Content-Disposition', 'inline; filename=' + filename); ////attachment
						res.setHeader('Content-type', 'application/pdf');
						out.stream.pipe(res);
					});


				}else{	
			    	client.release();	
					res.json({"code" : 101, "menssage" : "Se produjo un error al obtener la información para generar el pdf22."});
				}			
									

			} catch(e) {
				res.json({"code" : 101, "menssage" : e.message});				
			}finally{
				if (db)	db.end();			
			}	
		}else{
			res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
		}		
	})();	
});*/

router.post('/visor/generar_pdf_inspeccionar_padron',(req,res) => { 
	(async () => {	//pdf-creator-node  pdfkit  jsreport

		if (req.session.inicioSesion) {

			if (req.session.permisoPdf === 1) {

				if (req.body.mapa && req.body.coordenadas) {
					//instancia de la base de datos
					var db= new postgres.Pool(config_postgres);
					try {

						var client = await db.connect();				

						//si no existe la tabla en posgres no hacer la consulta.	
						var query = escapee('SELECT p.*, '+
						' d."NOM_POBL" AS nom_poblacion, '+
						' c."dg_tipcat" AS categoria '+
						' FROM "Culiacan_Join" p '+
						' INNER JOIN a_pobl d ON p.dg_pobl::int = d."CVE_POBL" '+
						' LEFT JOIN construccion c ON p.dg_ccat = c.dg_ccat '+
						' WHERE '+
						'	st_contains('+
						'		p.geom,'+
						'		ST_GeomFromText(\'POINT(%s)\',32613)'+
						')', req.body.coordenadas);
						
						var result = await client.query(query);

						if (result.rows.length > 0) {
					    	client.release();

							var construccion = '';
							for (var i = 0; i < result.rows.length; i++) {
								if(result.rows[i].categoria !== null){
									if (construccion !== '') {
										construccion+= ',' + result.rows[i].categoria;
									}else{
										construccion+= result.rows[i].categoria;
									}
								}
							}

							const fechaActual = new Date(dateTime.create().format('Y-m-d H:M:S'));
							const content = `
								<!doctype html>
								    <html>
								       <head>
								            <meta charset="utf-8">
								            <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">							    
								        	<style>

								        		body{
								        			font-family: "Times New Roman", Times, serif;
								        		}

								        		p{
								        			font-size: 15px;
								        		}

								        		strong{
								        			font-size: 15px;
								        		}

								        		#encabezado p{
								        			font-size: 17px;
								        		}

								        		#encabezado strong{
								        			font-size: 17px;
								        		}

								        		.table-bordered1 {
												    border: 1px solid #000;
												}

												.table td, .table th {
												    border-top: 1px solid #000;
												}
								        	</style>
								        </head>
								        <body>
								        <div id="header"></div>
							            <div id="encabezado">
							            	<img height="110px" width="75px" src="client/recursos/img/logo_direccion_catastro.jpg" style="position: absolute; top: 0;">
								
											<p class="text-center">
							                	<strong>
								                	AYUNTAMIENTO DE CULIACÁN<BR>
								                	DIRECCIÓN DE CATASTRO<BR>
								                	DICTAMEN DE PREDIOS URBANOS
							                	</strong>
							                </p>	
							                <p class="text-center">
							                	<strong>
								                	INSPECCIÓN
							                	</strong>
							                </p>
							                <img height="70px" width="120px" src="client/recursos/img/logo_c.png" style="position: absolute; top: 0; right: 95px;">
											<img height="120px" width="90px" src="client/recursos/img/logo_v.png" style="position: absolute; top: 0; right: 0;">
							            </div>
								        <div style="margin-top: 10mm;">

							            	<p style="margin: 0;">
							                	<strong>
								                	PROPIETARIO:
							                	</strong>
							                	`+result.rows[0].nom_comp_t+`
							                </p>

							               	<p style="margin: 0;">
							                	<strong>
								                	FECHA:
							                	</strong>
							                	`+fechaActual.getDate()+`/`+(fechaActual.getMonth()+1)+`/`+fechaActual.getFullYear()+`
							                </p>
							            	
				            				<p style="margin: 0;">
							                	<strong>
								                	CLAVE:
							                	</strong>
							                	`+result.rows[0].dg_cat+`
							                </p>

							                <p style="margin: 0;">
							                	<strong>
								                	POBLACIÓN:
							                	</strong>
							                	`+result.rows[0].nom_poblacion+`
							                </p>

							                <p style="margin: 0;">
							                	<strong>
								                	FRACCIONAMIENTO O COLONIA:
							                	</strong>
							                	`+result.rows[0].ubi_pred+`
							                </p>

							                <p style="margin: 0;">
							                	<strong>
								                	SUPERFICIE DE TERRENO:
							                	</strong>
							                	`+result.rows[0].sup_terr+` M2
							                </p>

							                <p style="margin: 0;">
							                	<strong>
								                	SUPERFICIE DE CONSTRUCCIÓN:
							                	</strong>
							                	`+result.rows[0].sup_const+` M2
							                </p>

							                <p style="margin: 0;">
							                	<strong>
								                	CATEGORIA ANTERIOR:
							                	</strong>
							                	
							                </p>

							                <p style="margin: 0;">
							                	<strong>
								                	CATEGORIA ACTUAL:
							                	</strong>
							                	`+construccion+`
							                </p>

							            </div>
				            			    			
							            <div style="margin-top: 5mm; margin-bottom: 5mm;" class="text-center">    		            						            	
							            		
											<table class="table table-bordered1">							  
											  <thead>
											    <tr>
												    <th class="text-center" style="border-bottom-color: #000; font-weight: 400;">
												      	<strong>
										                	RESULTADO DE LA INSPECCIÓN
									                	</strong>
									                </th>
											    </tr>
											  </thead>

											  <tbody>
											    <tr>
											      <td style="height: 40mm;"></td>
											    </tr>
											  </tbody>
											</table>


											<div style="margin-top: 15mm;">
												<p style="margin: 0;">
													<strong>
									                	_____________________________________
								                	</strong>
								                </p>
					            				<p style="margin: 0;">
						            				<strong>
									                	FIRMA DEL INSPECTOR
								                	</strong>
							                	</p>
				            				</div>
							            </div>

							            <div style="page-break-before: always;"></div>

							            <div class="text-center">    		      

							            	<img src="`+req.body.mapa+`" >

							            </div>
					
								        </body>
								    </html>
							`;

							jsreport.render({
								template: {
								  content: content,							 
								  engine: 'handlebars',
								  recipe: 'chrome-pdf', //html  chrone-pdf
								  chrome: {
								  	scale: 1,
								  	displayHeaderFooter: true,
							        format: 'Letter', //Letter
							        printBackground: true,
							        marginTop: '8mm',
							        marginRight: '8mm',
							        marginBottom: '4mm',
							        marginLeft: '8mm'
								  }
								}
							}).then((out)  => {
								var filename = "consultaPadron.pdf"; 					
								res.setHeader('Content-Disposition', 'inline; filename=' + filename); ////attachment
								res.setHeader('Content-type', 'application/pdf');
								out.stream.pipe(res);
							});


						}else{	
					    	client.release();	
							res.json({"code" : 101, "menssage" : "Se produjo un error al obtener la información para generar el pdf22."});
						}			
											

					} catch(e) {
						res.json({"code" : 101, "menssage" : e.message});				
					}finally{
						if (db)	db.end();			
					}	
				}else{
					res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
				}	

			}else{
				res.json({"code" : 101, "menssage" : "No tiene permiso para generar el PDF."});
			}	

		}else{
			res.json({"code" : 101, "menssage" : "Se necesita iniciar sesión."});
		}	
	})();	
});

router.get('/visor/consulta_padron',(req,res) => { 
	(async () => {

		if (req.session.inicioSesion) {

			if (req.query.p1) {
				//instancia de la base de datos
				var db= new postgres.Pool(config_postgres);
				try {
					var resultado = {};
					var client = await db.connect();				

					//si no existe la tabla en posgres no hacer la consulta.	
					const queryGeometria = escapee('SELECT id, public.ST_AsGeoJSON(geom,6) AS geometry FROM "Culiacan_Join"'+
					' WHERE '+
					'	st_contains('+
					'		geom,'+
					'		ST_GeomFromText(\'POINT(%s)\',32613)'+
					')', req.query.p1);


					var resultGeometria = await client.query(queryGeometria);
					if (resultGeometria.rows.length > 0) {
						resultado['Geometry'] = resultGeometria.rows[0].geometry;

						const queryUbicacion = escapee(`SELECT 
							AC."CALLE" AS "Calle",
							AU."NUM_OFIC_U" AS "Número Oficial",
							ACL."COLONIA" AS "Colonia",
							C.nom_mun AS "Municipio",
							CP."NOM_POBL" AS "Población",
							CJ.dg_ctel AS "Cuartel",
							CJ.dg_manz AS "Manzana",
							CJ.dg_pred AS "Número Predio",
							CJ.dg_unid AS "Número Unidad"
							FROM "Culiacan_Join" AS CJ 
							INNER JOIN culiacan as C ON CJ.dg_mpio = C.dg_mpio 
							INNER JOIN "a_pobl" as CP ON CJ.cve_pobl::int = CP."CVE_POBL" 
							INNER JOIN "a_calle" AS AC ON CJ.cve_pobl::int = AC."CVE_POBL" AND CJ.cve_calle::int = AC."CVE_CALLE" 
							INNER JOIN "a_col" AS ACL ON CJ.cve_pobl::int = ACL."CVE_POBL" AND CJ.cve_col::int = ACL."CVE_COL" 
							INNER JOIN "a_unid" AS AU 
							ON CJ.cve_pobl::int = AU."CVE_POBL" 
							AND CJ.num_ctel::int = AU."NUM_CTEL" 
							AND CJ.num_manz::int = AU."NUM_MANZ" 
							AND CJ.num_pred::int = AU."NUM_PRED" 
							AND CJ.num_unid::int = AU."NUM_UNID" 
							WHERE CJ.id = %s 
						`, resultGeometria.rows[0].id);


						const resultUbicacion = await client.query(queryUbicacion);

						if (resultUbicacion.rows.length > 0) {

							resultado['Ubicación'] = resultUbicacion.rows[0];
						}else{
							resultado['Ubicación'] = null;
						}


						//consultamos la informacion de propietarios
						const queryPropietarios = escapee(`SELECT 
							AP."TIP_PERS" AS "Tipo Persona",
							AP."APE_PAT" AS "Apellido Paterno",
							AP."APE_MAT" AS "Apellido Materno",
							AP."NOM_PROP" AS "Nombre(s)"
							FROM "Culiacan_Join" AS CJ						
							INNER JOIN "a_unid" AS AU 
							ON CJ.cve_pobl::int = AU."CVE_POBL" 
							AND CJ.num_ctel::int = AU."NUM_CTEL"
							AND CJ.num_manz::int = AU."NUM_MANZ"
							AND CJ.num_pred::int = AU."NUM_PRED"
							AND CJ.num_unid::int = AU."NUM_UNID"
							INNER JOIN "a_prop" AS AP ON AU."CVE_PROP" = AP."CVE_PROP" 
							WHERE CJ.id = %s 
						`, resultGeometria.rows[0].id);


						const resultPropietarios = await client.query(queryPropietarios);

						if (resultPropietarios.rows.length > 0) {

							resultado['Propietarios'] = resultPropietarios.rows[0];
						}else{
							resultado['Propietarios'] = null;
						}


						//consultamos la informacion de superficies
						const querySuperficies = escapee(`SELECT 
							AU."SUP_TERR" AS "Superficie de Terreno",
							AU."SUP_CONST" AS "Superficie de Construcción",
							AU."NUM_FTES" AS "Número de Frentes",
							AU."LON_FTE" AS "Longitud Frente(s)",
							AU."LON_FONDO" AS "Longitud Fondo"
							FROM "Culiacan_Join" AS CJ						
							INNER JOIN "a_unid" AS AU 
							ON CJ.cve_pobl::int = AU."CVE_POBL" 
							AND CJ.num_ctel::int = AU."NUM_CTEL"
							AND CJ.num_manz::int = AU."NUM_MANZ"
							AND CJ.num_pred::int = AU."NUM_PRED"
							AND CJ.num_unid::int = AU."NUM_UNID"
							WHERE CJ.id = %s 
						`, resultGeometria.rows[0].id);


						const resultSuperficies = await client.query(querySuperficies);

						if (resultSuperficies.rows.length > 0) {

							resultado['Superficies'] = resultSuperficies.rows[0];
						}else{
							resultado['Superficies'] = null;
						}


						//consultamos la informacion de valores
						const queryValores = escapee(`SELECT 
							AU."VAL_TERR" AS "Valor del Terreno",
							AU."VAL_CONST" AS "Valor de la Construcción"
							FROM "Culiacan_Join" AS CJ						
							INNER JOIN "a_unid" AS AU 
							ON CJ.cve_pobl::int = AU."CVE_POBL" 
							AND CJ.num_ctel::int = AU."NUM_CTEL"
							AND CJ.num_manz::int = AU."NUM_MANZ"
							AND CJ.num_pred::int = AU."NUM_PRED"
							AND CJ.num_unid::int = AU."NUM_UNID"
							WHERE CJ.id = %s 
						`, resultGeometria.rows[0].id);


						const resultValores = await client.query(queryValores);

						if (resultValores.rows.length > 0) {

							resultado['Valores'] = resultValores.rows[0];
						}else{
							resultado['Valores'] = null;
						}

						//consultamos la informacion de Situacion del predio
						const querySituacionPredio = escapee(`SELECT 
							AU."FEC_ALTA" AS "Fecha Alta",
							AR."DES_REGIM" AS "Tipo Régimen",
							(CASE WHEN AU."TIP_IMPTO" = 1 THEN 'BALDÍO' ELSE 'CONSTRUÍDO' END) AS "Tipo Impuesto",
							(CASE WHEN AU."STT_REST" = 'S' THEN 'CON RESTRICCIÓN' ELSE 'SIN RESTRICCIÓN' END) AS "Tipo Restricción"
							FROM "Culiacan_Join" AS CJ						
							INNER JOIN "a_unid" AS AU 
							ON CJ.dg_pobl::int = AU."CVE_POBL" 
							AND CJ.dg_ctel::int = AU."NUM_CTEL"
							AND CJ.dg_manz::int = AU."NUM_MANZ"
							AND CJ.dg_pred::int = AU."NUM_PRED"
							AND CJ.dg_unid::int = AU."NUM_UNID"
							INNER JOIN "a_regim" AS AR ON AR."CVE_REGIM" = AU."CVE_REGIM" 
							WHERE CJ.id = %s 
						`, resultGeometria.rows[0].id);
						
						const resultSituacionPredio = await client.query(querySituacionPredio);

						if (resultSituacionPredio.rows.length > 0) {

							resultado['Situación del Predio'] = resultSituacionPredio.rows[0];
						}else{
							resultado['Situación del Predio'] = null;
						}

						//consultamos la informacion de Servicios
						const queryServicios = escapee(`SELECT 
							AU."STS_SERVIC" AS servicios
							FROM "Culiacan_Join" AS CJ						
							INNER JOIN "a_unid" AS AU 
							ON CJ.dg_pobl::int = AU."CVE_POBL" 
							AND CJ.dg_ctel::int = AU."NUM_CTEL"
							AND CJ.dg_manz::int = AU."NUM_MANZ"
							AND CJ.dg_pred::int = AU."NUM_PRED"
							AND CJ.dg_unid::int = AU."NUM_UNID"
							WHERE CJ.id = %s 
						`, resultGeometria.rows[0].id);

						const resultServicios = await client.query(queryServicios);

						if (resultServicios.rows.length > 0) {

							const datoServicio = resultServicios.rows[0].servicios; //'13111111100'
							const grupoServicios = [
												 'AGUA',
												 'ALUMBRADO',
												 'BANQUETA',
												 'DRENAJE', 
												 'DESAGUE', 
												 'ENERGIA', 
												 'GUARNICION', 
												 'PAVIMENTO', 
												 'TOMA DE AGUA'];

							var arrayServicios = [];

							for (var i = 0; i < datoServicio.length; i++) {
								if (i === 1 || i === 7) {

									if (i === 1) {
										var des_servicio = null;
										switch(datoServicio[i]){
											case '0':
												des_servicio = 'ALUMBRADO';
												break;
											case '1':
												des_servicio = 'MERCURIAL';
												break;
											case '2':
												des_servicio = 'FLUORESCENTE';
												break;
											case '3':
												des_servicio = 'INCANDESCENTE';
												break;
											case '4':
												des_servicio = 'NO HAY';
												break;
											case 'A':
												des_servicio = 'MERCURIAL-FLUORESCENTE';
												break;
											case 'B':
												des_servicio = 'MERCURIAL-INCANDESCENTE';
												break;
											case 'C':
												des_servicio = 'MERCURIAL-NO HAY';
												break;
											case 'D':
												des_servicio = 'FLUORESCENTE-INCANDESCENTE';
												break;
											default:
												des_servicio = null;
												break;

										}
										if (des_servicio !== null) {
											arrayServicios.push({"des_gpo_serv" : grupoServicios[i], "des_serv" : des_servicio});
										}
									}else{

										var des_servicio = null;
										switch(datoServicio[i]){
											case '0':
												des_servicio = 'PAVIMENTO';
												break;
											case '1':
												des_servicio = 'CONCRETO';
												break;
											case '2':
												des_servicio = 'ASFALTO';
												break;
											case '3':
												des_servicio = 'EMPEDRADO';
												break;
											case '4':
												des_servicio = 'NATURAL';
												break;
											case 'A':
												des_servicio = 'CONCRETO-ASFALTO';
												break;
											case 'B':
												des_servicio = 'CONCRETO-EMPEDRADO';
												break;
											case 'C':
												des_servicio = 'CONCRETO-NATURAL';
												break;
											case 'D':
												des_servicio = 'ASFALTO-EMPEDRADO';
												break;
											case 'E':
												des_servicio = 'ASFALTO-NATURAL';
												break;
											case 'F':
												des_servicio = 'EMPEDRADO-NATURAL';
												break;
											default:
												des_servicio = null;
												break;

										}
										if (des_servicio !== null) {
											arrayServicios.push({"des_gpo_serv" : grupoServicios[i], "des_serv" : des_servicio});
										}
									}

								}else{
									if (datoServicio[i] > 0) {
										arrayServicios.push({"des_gpo_serv" : grupoServicios[i], "des_serv" : grupoServicios[i]});
									}
								}
							}
							resultado['Servicios'] = arrayServicios;
						}else{
							resultado['Servicios'] = null;
						}

						//consultamos la informacion de Usos
						const queryUsos = escapee(`SELECT 
							AG."DES_GPO_US" AS "Grupo Uso",
							AG."DES_GPO_US" AS "Uso"
							FROM "Culiacan_Join" AS CJ						
							INNER JOIN "a_uso_u" AS AU 							
							ON CJ.dg_pobl::int = AU."CVE_POBL" 
							AND CJ.dg_ctel::int = AU."NUM_CTEL"
							AND CJ.dg_manz::int = AU."NUM_MANZ"
							AND CJ.dg_pred::int = AU."NUM_PRED"
							AND CJ.dg_unid::int = AU."NUM_UNID"
							AND CJ.cve_gpo_us = AU."CVE_GPO_USO"
							INNER JOIN "a_gUso" AS AG ON AG."CVE_GPO_US" = AU."CVE_GPO_USO" 
							WHERE CJ.id = %s 
						`, resultGeometria.rows[0].id);

						const resultUsos = await client.query(queryUsos);

						if (resultUsos.rows.length > 0) {

							resultado['Usos'] = resultUsos.rows[0];
						}else{
							resultado['Usos'] = null;
						}

						//consultamos la informacion de Construcciones
						const queryConstrucciones = escapee(`SELECT 
							AC."NUM_CONST" AS "Número de Construcción",
							AC."SUP_CONST" AS "Superficie de Construcción",
							AC."CVE_CAT_CO" AS "Categoría de Construcción",
							CONCAT('EDO-', AC."CVE_EDO_CO") AS "Estado de Conservación",
							AC."EDD_CONST" AS "Edad de la Construcción",
							AC."FEC_CONST" AS "Fecha de Construcción"
							FROM "Culiacan_Join" AS CJ						
							INNER JOIN "a_const" AS AC							
							ON CONCAT(CJ.dg_pobl, CJ.dg_ctel, CJ.dg_manz, CJ.dg_pred, CJ.dg_unid) = AC.clave 
							WHERE CJ.id = %s 
						`, resultGeometria.rows[0].id);

						const resultConstrucciones = await client.query(queryConstrucciones);

						if (resultConstrucciones.rows.length > 0) {

							resultado['Construcciones'] = resultConstrucciones.rows;
						}else{
							resultado['Construcciones'] = null;
						}

						res.json({"code" : 200, "data" : resultado});
						
					}else{
						res.json({"code" : 200, "data" : null});
					}
											
				    client.release();	

				} catch(e) {
					res.json({"code" : 101, "menssage" : e.message});				
				}finally{
					if (db)	db.end();			
				}	
			}else{
				res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
			}	

		}else{
			res.json({"code" : 101, "menssage" : "Se necesita iniciar sesión."});
		}	
			

	})();											
});

router.get('/visor/inspeccionar_padron',(req,res) => { 
	(async () => {

		if (req.session.inicioSesion) {

			if (req.query.p1) {
				//instancia de la base de datos
				var db= new postgres.Pool(config_postgres);
				try {
					var client = await db.connect();				

					//si no existe la tabla en posgres no hacer la consulta.	
					const queryGeometria = escapee('SELECT id, public.ST_AsGeoJSON(geom,6) AS geometry FROM "Culiacan_Join"'+
					' WHERE '+
					'	st_contains('+
					'		geom,'+
					'		ST_GeomFromText(\'POINT(%s)\',32613)'+
					')', req.query.p1);


					var resultGeometria = await client.query(queryGeometria);
					if (resultGeometria.rows.length > 0) {
						res.json({"code" : 200, "data" : resultGeometria.rows[0]});						
					}else{
						res.json({"code" : 200, "data" : null});
					}
											
				    client.release();	

				} catch(e) {
					res.json({"code" : 101, "menssage" : e.message});				
				}finally{
					if (db)	db.end();			
				}	
			}else{
				res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
			}		

		}else{
			res.json({"code" : 101, "menssage" : "Se necesita iniciar sesión."});  
		}
			

	})();											
		
});

router.post('/visor/obtener_features_snap',(req,res) => { 
	(async () => {

		if (req.session.inicioSesion) {

			if (req.body.geometry && req.body.scale) {		
				
				if (req.body.scale < 2254) {			
					//instancia de la base de datos
					var db= new postgres.Pool(config_postgres);
					try {
						var client = await db.connect();

						var canceladoClient = false;
						req.on('close', function (err){
							canceladoClient = true;
					    });

						var resultado = [];
						for (var i = 0; i < LAYERS_SNAP.length; i++) {
							if (!canceladoClient) {

								var query = 'SELECT public.ST_AsGeoJSON(geom,6) AS geometry'+
								' FROM "'+LAYERS_SNAP[i]+'"'+
								' WHERE '+
								'	st_contains('+
								'		ST_GeomFromGeoJSON(\''+req.body.geometry+'\'),'+
								'		geom'+
								')';

								var result = await client.query(query);

								if (result.rows.length > 0) {
									resultado.push({"capa" : LAYERS_SNAP[i], "data" : result.rows});
								}else{
									resultado.push({"capa" : LAYERS_SNAP[i], "data" : null});
								}	

							}else{
								return;
							}
							
						}

						if (canceladoClient) {
							res.status(403).end();
						}else{
							res.json({"code" : 200, "data" : resultado});
						}
						client.release();
					} catch(e) {
						client.release();
						res.json({"code" : 101, "menssage" : e.message});				
					}finally{
						if (db)	db.end();			
					}

				}else{
					res.json({"code" : 101, "menssage" : "LA ESCALA NO ESTA EN EL RANGO ADECUADO."});
				}	
			}else{
				res.json({"code" : 101, "menssage" : "LOS PARAMETROS NO SON CORRECTOS."});
			}	

		}else{
			res.json({"code" : 101, "menssage" : "Se necesita iniciar sesión."});
		}					

	})();											
});

module.exports = router;